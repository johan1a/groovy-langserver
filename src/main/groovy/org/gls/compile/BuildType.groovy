package org.gls.compile

/**
 * Created by johan on 4/15/18.
 */
interface BuildType {

    List<Dependency> resolveDependencies()
}
