package org.gls.compile

import groovy.transform.TypeChecked
import groovy.util.logging.Slf4j

@TypeChecked
@Slf4j
@SuppressWarnings("CatchException")
class GradleBuild implements BuildType {

    public static final String JAVADOC_NAME = "javadoc.jar"
    public static final String SOURCES_NAME = "sources.jar"
    String userHome = System.getProperty("user.home")

    String gradleHome = userHome + "/.gradle/"
    String mavenHome = userHome + "/.m2/"
    List<String> libraries = [gradleHome, '/usr/share/grails', mavenHome]

    URI configPath

    GradleBuild(URI configPath) {
        this.configPath = configPath
    }

    @Override
    List<Dependency> resolveDependencies() {
        try {
            long start = System.currentTimeMillis()

            log.info("Parsing jars from gradle")
            List<Dependency> dependencies = parseDependencies()
            log.info("Parsed ${dependencies.size()} dependencies from gradle config)}")
            List<Dependency> dependenciesWithFilePaths = findJarLocations(dependencies)

            BigDecimal elapsed = (System.currentTimeMillis() - start) / 1000.0
            log.info("Found ${dependenciesWithFilePaths.size()} jars on filesystem in $elapsed seconds")
            return dependenciesWithFilePaths
        } catch (Exception e) {
            log.error("Error", e)
            Collections.emptyList()
        }
    }

    @SuppressWarnings(["NestedBlockDepth", "ThrowRuntimeException"])
    List<Dependency> findJarLocations(List<Dependency> dependencies) {
        List<Dependency> unresolved = []
        unresolved.addAll(dependencies)

        List<Dependency> result = new LinkedList<>()
        try {
            libraries.each { library ->
                File directory = new File(library)
                log.info("Searching for jars in: ${directory.absolutePath}")
                if (directory.isDirectory()) {
                    directory.eachFileRecurse { File file ->
                        log.debug("Considering file: $file")
                        Dependency dependency = unresolved.find { dependency ->
                            !result.contains(dependency) &&
                                    (jarNameMatch(file, dependency) || sourceJarNameMatch(file, dependency))
                        }

                        if (dependency) {
                            updateDependencyinfo(result, dependency, file)
                            if (dependency.jarPath && dependency.sourceJarFileName) {
                                unresolved.remove(dependency)
                                if (result.size() == dependencies.size()) {
                                    log.info("Found every dependency. Stopping search...")
                                    throw new RuntimeException("Finished")
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception ignored) {
        }
        return result
    }

    private void updateDependencyinfo(List<Dependency> resultList, Dependency dependency, File file) {
        if (!resultList.contains(dependency) && jarNameMatch(file, dependency)) {
            dependency.jarPath = new URI(file.absolutePath)
            resultList.add(dependency)
        }
        if (sourceJarNameMatch(file, dependency)) {
            dependency.sourcesJarPath = new URI(file.absolutePath)
        }
    }

    static boolean sourceJarNameMatch(File file, Dependency dependency) {
        String fileName = file.name
        if (file.isDirectory() ||
                !fileName.endsWith(SOURCES_NAME) ||
                fileName.endsWith(JAVADOC_NAME)) {
            return false
        }
        fileName.contains(dependency.sourceJarFileName)
    }

    static boolean jarNameMatch(File file, Dependency dependency) {
        String fileName = file.name
        if (file.isDirectory() ||
                !fileName.endsWith(".jar") ||
                fileName.endsWith(SOURCES_NAME) ||
                fileName.endsWith(JAVADOC_NAME)) {
            return false
        }
        fileName.contains(dependency.jarFileName)
    }

    List<Dependency> parseDependencies() {
        List<String> gradleOutput = callGradle()
        List<Dependency> dependencies = new LinkedList<>()

        gradleOutput.collect { line ->
            parseJarName(line).map { dependencies.add(it) }
        }
        dependencies.unique()
    }

    List<String> callGradle() {
        StringBuilder sout = new StringBuilder(), serr = new StringBuilder()
        Process proc = './gradlew -q dependencies'.execute()
        proc.consumeProcessOutput(sout, serr)
        proc.waitForOrKill(10000)
        sout.toString().split(System.lineSeparator()).toList()
    }

    static Optional<Dependency> parseJarName(String line) {
        try {
            if (isDependencyLine(line)) {
                return Optional.of(parseSplitJarName(line))
            }
        } catch (Exception e) {
            log.error("Error while parsing $line", e)
        }
        return Optional.empty()
    }

    static boolean isDependencyLine(String line) {
        return line.contains("+---") || line.contains("\\---")
    }

    static Dependency parseSplitJarName(String line) {
        String trimmed = trimLine(line)
        String[] split = trimmed.split(":")
        Dependency dependency = new Dependency(
                group: split[0],
                name: split[1],
                version: parseVersion(split[2])
        )
        return dependency
    }

    static String parseVersion(String version) {
        String arrow = "->"
        if (version.contains(arrow)) {
            return version.split(arrow)[1].trim()
        }
        return version.trim()
    }

    static String trimLine(String line) {
        line.split("---")[1].replace("(*)", "").trim()
    }
}
