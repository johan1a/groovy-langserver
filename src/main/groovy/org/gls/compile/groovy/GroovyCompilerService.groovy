package org.gls.compile.groovy

import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.eclipse.lsp4j.Diagnostic
import org.gls.compile.CompilerConfig
import org.gls.compile.Dependency
import org.gls.lsp.UriUtils
import org.gls.compile.CompilerService
import org.gls.lang.LanguageService

@Slf4j
@CompileStatic
class GroovyCompilerService {

    private static final String GROOVY_FILE_EXTENSION = ".groovy"
    private static final String JAVA_FILE_EXTENSION = ".java"

    List<URI> sourcePaths

    LanguageService service = new LanguageService()
    URI rootUri
    CompilerConfig compilerConfig

    CompilerService compilerService

    GroovyCompilerService(URI rootUri, LanguageService service, CompilerConfig compilerConfig) {
        this.rootUri = rootUri
        this.compilerConfig = compilerConfig
        sourcePaths = [
                UriUtils.appendURI(rootUri, "/src/main/groovy"),
                UriUtils.appendURI(rootUri, "/grails-app/domain"),
                UriUtils.appendURI(rootUri, "/grails-app/services"),
        ]
        if (compilerConfig.scanAllSubDirs) {
            sourcePaths = [rootUri]
        }
        log.info "sourcePaths: ${sourcePaths}"

        this.service = service

        compilerService = new CompilerService(languageService: service,
                compiler: new GroovyCompiler(),
                fileExtensions: [GROOVY_FILE_EXTENSION, JAVA_FILE_EXTENSION])
    }

    Map<String, List<Diagnostic>> compile(List<Dependency> dependencies = [], Map<String, String> changedFiles = [:]) {
        return compilerService.compile(dependencies, sourcePaths, changedFiles)
    }

}
