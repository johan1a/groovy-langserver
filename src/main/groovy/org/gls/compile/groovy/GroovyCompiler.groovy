package org.gls.compile.groovy

import groovy.util.logging.Slf4j
import org.codehaus.groovy.ast.ModuleNode
import org.codehaus.groovy.control.CompilationUnit
import org.codehaus.groovy.control.CompilerConfiguration
import org.codehaus.groovy.control.Phases
import org.gls.compile.CompilationFileContext
import org.gls.compile.FileFinder
import org.gls.compile.Compiler
import org.gls.lang.LanguageService

@Slf4j
class GroovyCompiler implements Compiler {

    private static final int CONFIGURATION_TOLERANCE = 1000_000

    FileFinder fileFinder = new FileFinder()

    void compile(LanguageService languageService, CompilationFileContext context) {
        List<File> notChanged = context.files.findAll { !context.changedFiles.keySet().contains(it.canonicalPath) }

        CompilerConfiguration configuration = new CompilerConfiguration()
        configuration.tolerance = CONFIGURATION_TOLERANCE
        configuration.recompileGroovySource = true

        CompilationUnit unit = new CompilationUnit(configuration)

        notChanged.each { unit.addSource(it) }
        context.changedFiles.each { path, name -> unit.addSource(path, name) }

        context.dependencies.each {
            unit.classLoader.addClasspath(it.jarPath.toString())
        }

        unit.compile(Phases.CLASS_GENERATION)

        unit.iterator().each { sourceUnit ->
            log.debug("compiling ${sourceUnit.name}")
            ModuleNode moduleNode = sourceUnit.AST

            String name = sourceUnit.name
            List<String> fileContents = fileFinder.getFileContent(name, context.changedFiles)
            GroovyCodeVisitor codeVisitor = new GroovyCodeVisitor(languageService, name, fileContents)
            moduleNode.visit(codeVisitor)
            moduleNode.classes.each { classNode ->
                codeVisitor.visitClass(classNode)
            }
        }
    }
}
