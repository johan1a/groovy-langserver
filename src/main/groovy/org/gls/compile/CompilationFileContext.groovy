package org.gls.compile

class CompilationFileContext {
    List<URI> sourcePaths
    Map<String, String> changedFiles = [:]
    List<Dependency> dependencies = []
    URI bundleURI
    URI bundleTempDirURI
    List<File> files
}

