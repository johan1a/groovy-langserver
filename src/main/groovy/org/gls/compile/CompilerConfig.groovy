package org.gls.compile

import groovy.transform.TypeChecked

@TypeChecked
class CompilerConfig {
    boolean scanAllSubDirs = true
    boolean scanDependencies = false
    boolean serializeLanguageService = true
}
