package org.gls.compile

import groovy.util.logging.Slf4j
import org.gls.lsp.UriUtils
import java.util.zip.ZipEntry
import java.util.zip.ZipFile

@Slf4j
class ZipFileExtractor {

    void extract(URI zipFileURI, URI tempDir) {
        log.debug("Extracting uri: ${zipFileURI}")
        ZipFile file = new ZipFile(new File(zipFileURI))
        file.entries().each { entry ->
            extractEntry(file, entry, tempDir)
        }
    }

    void extractEntry(ZipFile zipFile, ZipEntry entry, URI tempDir) {
        URI fileURI = UriUtils.appendURI(tempDir, entry.name)
        if (entry.directory) {
            new File(fileURI).mkdirs()
        } else {
            createParentDirectories(fileURI)
            new File(fileURI).text = zipFile.getInputStream(entry).text
        }
    }

    void createParentDirectories(URI uri) {
        new File(UriUtils.parentDir(uri)).mkdirs()
    }

}
