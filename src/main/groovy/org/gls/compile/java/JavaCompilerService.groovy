package org.gls.compile.java

import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.eclipse.lsp4j.Diagnostic
import org.gls.compile.CompilerConfig
import org.gls.compile.CompilerService
import org.gls.lang.LanguageService
import org.gls.lsp.UriUtils

@Slf4j
@CompileStatic
class JavaCompilerService {

    private static final String JAVA_FILE_EXTENSION = ".java"

    List<URI> sourcePaths

    LanguageService service = new LanguageService()
    URI rootUri
    CompilerConfig compilerConfig

    CompilerService compilerService

    JavaCompilerService(URI rootUri, LanguageService service, CompilerConfig compilerConfig) {
        this.rootUri = rootUri
        this.compilerConfig = compilerConfig
        sourcePaths = [
                UriUtils.appendURI(rootUri, "/src/main/java"),
        ]
        if (compilerConfig.scanAllSubDirs) {
            sourcePaths = [rootUri]
        }
        log.info "sourcePaths: ${sourcePaths}"

        this.service = service

        compilerService = new CompilerService(languageService: service,
                compiler: new CustomJavaCompiler(),
                fileExtensions: [JAVA_FILE_EXTENSION]
        )
    }

    Map<String, List<Diagnostic>> compile(Map<String, String> changedFiles = [:]) {
        return compilerService.compile([], sourcePaths, changedFiles)
    }

    Map<String, List<Diagnostic>> compileSourceBundle(URI bundleURI) {
        compilerService.compileSourceBundle(bundleURI)
    }
}
