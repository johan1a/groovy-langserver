package org.gls.compile.java

class VisitorState {
    String packageName
    URI bundleURI
    URI tempFileURI
    URI bundleTempDirURI

    URI getSourceFileURI() {
        tempFileURI
    }

    List<String> fileContents
}
