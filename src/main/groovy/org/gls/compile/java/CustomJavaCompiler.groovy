package org.gls.compile.java

import static com.github.javaparser.ParserConfiguration.LanguageLevel.JAVA_11

import com.github.javaparser.JavaParser
import com.github.javaparser.ParseProblemException
import com.github.javaparser.ast.CompilationUnit
import groovy.util.logging.Slf4j
import org.gls.compile.CompilationFileContext
import org.gls.compile.Compiler
import org.gls.compile.FileFinder
import org.gls.lang.LanguageService
import org.gls.lsp.UriUtils

@Slf4j
class CustomJavaCompiler implements Compiler {

    FileFinder fileFinder = new FileFinder()

    @Override
    void compile(LanguageService languageService, CompilationFileContext context) {
        JavaParser.staticConfiguration.languageLevel = JAVA_11

        context.files.each {
            if (it.isFile()) {
                try {
                    CompilationUnit compilationUnit = JavaParser.parse(it)
                    List<String> fileContents = fileFinder.getFileContent(it.path, context.changedFiles)
                    JavaCodeVisitor visitor = new JavaCodeVisitor(languageService: languageService)
                    compilationUnit.accept(visitor, new VisitorState(bundleURI: context.bundleURI,
                            bundleTempDirURI: context.bundleTempDirURI,
                            tempFileURI: UriUtils.toInternalUri(it),
                            fileContents: fileContents
                    ))
                } catch (ParseProblemException e) {
                    log.error("Exception when parsing file: $it", e)
                }
            }
        }
    }
}
