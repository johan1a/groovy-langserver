package org.gls.compile.java

import com.github.javaparser.ast.PackageDeclaration
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration
import com.github.javaparser.ast.visitor.VoidVisitorAdapter
import groovy.util.logging.Slf4j
import org.gls.lang.LanguageService
import org.gls.lang.definition.ClassDefinition

@Slf4j
class JavaCodeVisitor extends VoidVisitorAdapter<VisitorState> {

    LanguageService languageService

    @Override
    void visit(final PackageDeclaration node, final VisitorState visitorState) {
        visitorState.packageName = node.name.toString()
        super.visit(node, visitorState)
    }

    @Override
    void visit(final ClassOrInterfaceDeclaration node, final VisitorState visitorState) {
        ClassDefinition classDefinition = new ClassDefinition(node, visitorState)
        languageService.addClassDefinition(classDefinition)
        super.visit(node, visitorState)
    }
}
