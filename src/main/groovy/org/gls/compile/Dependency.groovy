package org.gls.compile

import groovy.transform.ToString

@ToString
@SuppressWarnings(["DuplicateNumberLiteral"])
class Dependency {

    String group
    String name
    String version

    URI jarPath
    URI sourcesJarPath

    String getJarFileName() {
        return "${name}-${version}.jar"
    }

    String getSourceJarFileName() {
        return "${name}-${version}-sources.jar"
    }

    boolean equals(Object o) {
        if (this.is(o)) {
            return true
        }
        if (getClass() != o.class) {
            return false
        }

        Dependency that = (Dependency) o

        if (group != that.group) {
            return false
        }
        if (jarPath != that.jarPath) {
            return false
        }
        if (name != that.name) {
            return false
        }
        if (sourcesJarPath != that.sourcesJarPath) {
            return false
        }
        return version == that.version
    }

    int hashCode() {
        int result
        result = (group != null ? group.hashCode() : 0)
        result = 31 * result + (name != null ? name.hashCode() : 0)
        result = 31 * result + (version != null ? version.hashCode() : 0)
        result = 31 * result + (jarPath != null ? jarPath.hashCode() : 0)
        result = 31 * result + (sourcesJarPath != null ? sourcesJarPath.hashCode() : 0)
        return result
    }
}
