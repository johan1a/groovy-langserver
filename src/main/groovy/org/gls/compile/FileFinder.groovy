package org.gls.compile

import groovy.transform.TypeChecked
import groovy.util.logging.Slf4j
import org.gls.lang.ClassPreprocessor

@Slf4j
@TypeChecked
class FileFinder {

    List<File> findFilesRecursive(List<URI> sourcePaths, Map<String, String> modifiedFiles,
                                  List<String> fileExtensions) {
        List<File> files = new LinkedList<>()
        sourcePaths.each {
            try {
                File basedir = new File(it)
                if (basedir.exists()) {
                    basedir.eachFileRecurse {
                        String filename = it.name
                        if (shouldAddLogField(it.path, filename)) {
                            addLogField(modifiedFiles, filename, it)
                        } else if (validExtension(filename, fileExtensions)) {
                            files.add(it)
                        }
                    }
                } else {
                    log.info("Directory $basedir does not exist, skipping...")
                }
            } catch (FileNotFoundException e) {
                log.debug("Source dir not found", e)
            }
        }
        return files
    }

    boolean validExtension(String filename, List<String> extensions) {
        extensions.any { filename =~ /.*${it}/ }
    }

    private void addLogField(Map<String, String> modifiedFiles, String filename, File file) {
        if (modifiedFiles.containsKey(filename)) {
            modifiedFiles[file.path] = ClassPreprocessor.addLogField(filename, modifiedFiles[filename])
        } else {
            String content = ClassPreprocessor.addLogField(filename, file.text)
            modifiedFiles[file.path] = content
        }
    }

    private boolean shouldAddLogField(String path, String filename) {
        path.contains("grails-app/") &&
                (filename =~ /.*Service\.groovy/ || filename =~ /.*Controller\.groovy/)
    }

    static List<String> getFileContent(String fileName, Map<String, String> changedFiles) {
        if (changedFiles.containsKey(fileName)) {
            return changedFiles.get(fileName).split('\n').toList() //TODO proper line end split
        }
        return new File(fileName).readLines()
    }

}
