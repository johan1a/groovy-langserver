package org.gls.compile

import groovy.util.logging.Slf4j
import org.codehaus.groovy.control.MultipleCompilationErrorsException
import org.eclipse.lsp4j.Diagnostic
import org.gls.lang.DiagnosticsParser
import org.gls.lang.LanguageService

@Slf4j
class CompilerService {

    private static final int MILLIS_PER_SECOND = 1000

    Compiler compiler

    LanguageService languageService

    List<String> fileExtensions

    Map<String, List<Diagnostic>> compile(List<Dependency> dependencies,
                                          List<URI> sourcePaths,
                                          Map<String, String> changedFiles = [:]) {
        CompilationFileContext context = new CompilationFileContext(
                sourcePaths: sourcePaths,
                changedFiles: changedFiles,
                dependencies: dependencies,
        )
        return compile(context)
    }

    Map<String, List<Diagnostic>> compileSourceBundle(URI bundleURI) {
        ConfigService.CONFIG_BASE_DIR
        ZipFileExtractor zipFileExtractor = new ZipFileExtractor()

        URI extractionDir = new ConfigService().getExtractionDirURI(bundleURI)

        zipFileExtractor.extract(bundleURI, extractionDir)
        CompilationFileContext context = new CompilationFileContext(
                sourcePaths: [extractionDir],
                bundleURI: bundleURI,
                bundleTempDirURI: extractionDir
        )
        return compile(context)
    }

    Map<String, List<Diagnostic>> compile(CompilationFileContext context) {
        FileFinder fileFinder = new FileFinder()
        context.files = fileFinder.findFilesRecursive(context.sourcePaths, context.changedFiles, fileExtensions)
        return compileInternal(context)
    }

    Map<String, List<Diagnostic>> compileInternal(CompilationFileContext context) {
        Map<String, List<Diagnostic>> diagnostics = [:]
        try {
            log.info("Starting compilation")
            long start = System.currentTimeMillis()

            compiler.compile(languageService, context)

            long elapsed = System.currentTimeMillis() - start
            log.info("Compilation done in ${elapsed / MILLIS_PER_SECOND}s")
        } catch (MultipleCompilationErrorsException e) {
            log.debug("Got MultipleCompilationErrorsException")
            diagnostics = DiagnosticsParser.getDiagnostics(e.errorCollector)
            if (diagnostics.isEmpty()) {
                log.error("Compilation error without diagnostics:", e)
            }
        } catch (NoClassDefFoundError e) {
            log.error("Compilation error:", e)
        }
        log.info("diagnostics size: ${diagnostics.size()}")
        diagnostics.each { log.debug(it.toString()) }
        return diagnostics
    }

}
