package org.gls.compile

import org.gls.lang.LanguageService

interface Compiler {

    void compile(LanguageService languageService, CompilationFileContext context)

}
