package org.gls.compile

import static org.gls.lsp.UriUtils.appendURI

import groovy.transform.TypeChecked
import groovy.util.logging.Slf4j
import org.gls.lsp.UriUtils
import org.gls.exception.ConfigException

import java.nio.file.Files
import java.nio.file.Paths

@TypeChecked
@Slf4j
class ConfigService {

    static final URI CONFIG_BASE_DIR = new URI("file://${System.getProperty("user.home")}/.langserver")
    static final URI BUNDLES_BASE_DIR = appendURI(CONFIG_BASE_DIR, "/bundles")

    private static final String DEFAULT_BUILD_CONFIG_FILE = "build.gradle"
    public static final String FILE = "file:"

    String configFileName = DEFAULT_BUILD_CONFIG_FILE

    BuildType buildType
    List<Dependency> dependencies

    @SuppressWarnings('CatchException')
    List<Dependency> getDependencies(URI rootUri) {
        try {
            buildType = buildType ?: getBuildType(rootUri)
            dependencies = buildType.resolveDependencies()
            return dependencies
        } catch (Exception e) {
            log.error('Error when resolving dependencies', e)
            return []
        }
    }

    static URI getConfigDir(URI rootUri) {
        String stringPath
        if (rootUri.scheme) {
            stringPath = Paths.get(rootUri).toString()
        } else {
            stringPath = rootUri.toString()
        }
        UriUtils.appendURI(CONFIG_BASE_DIR, stringPath)
    }

    BuildType getBuildType(URI rootUri) {
        log.info('Searching for gradle...')
        URI gradlePath = UriUtils.appendURI(rootUri, configFileName)
        if (Files.exists(Paths.get(gradlePath))) {
            log.info 'Found gradle.'
            return new GradleBuild(gradlePath)
        }
        throw new ConfigException('Build type not supported.')
    }

    URI findJDKSourceURI() {
        log.info("Searching for JDK")
        List<String> executable = runCommand("which java")
        if (executable.isEmpty() || !executable.first()) {
            return null
        }
        log.debug("Executable: $executable")

        List<String> followedLink = runCommand("readlink -f ${executable[0]}")
        if (followedLink.isEmpty() || !executable.first()) {
            return null
        }
        log.debug("Followedlink: $followedLink")

        List<String> dirNameOutput = runCommand("dirname ${followedLink[0]}")
        if (dirNameOutput.isEmpty() || !executable.first()) {
            return null
        }
        log.debug("dirNameOutput: $dirNameOutput")

        String pathToBin = dirNameOutput[0]
        File binDir = new File(pathToBin)
        if (!binDir) {
            return null
        }
        File jdkRootDir = binDir.parentFile

        URI possibleURI = appendURI(jdkRootDir.toURI(), "lib/src.zip")
        File possibleResult = new File(possibleURI)

        log.debug("possibleURI: $possibleURI")

        if (possibleResult.exists()) {
            log.info("Found jdk sources at: $possibleResult")
            return possibleURI
        }

        possibleURI = appendURI(jdkRootDir.toURI(), "src.zip")
        possibleResult = new File(possibleURI)

        log.debug("possibleURI: $possibleURI")

        if (possibleResult.exists()) {
            log.info("Found jdk sources at: $possibleResult")
            return possibleURI
        }
        return null
    }

    private List<String> runCommand(String command) {
        StringBuilder sout = new StringBuilder(), serr = new StringBuilder()
        Process proc = command.execute()
        proc.consumeProcessOutput(sout, serr)
        proc.waitForOrKill(5000)
        if (serr) {
            log.error(serr.toString())
        }
        sout.toString().split(System.lineSeparator()).toList()
    }

    URI getExtractionDirURI(URI bundleURI) {
        String uriString = UriUtils.parentDir(bundleURI)
        String parentDir = uriString.replace("file://", "")
                .replace("file:/", "")
        UriUtils.appendURI(ConfigService.BUNDLES_BASE_DIR, parentDir)
    }

}
