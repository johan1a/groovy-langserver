package org.gls.lang.definition

import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration
import groovy.transform.ToString
import groovy.transform.TypeChecked
import groovy.util.logging.Slf4j
import org.codehaus.groovy.ast.ClassNode
import org.gls.compile.java.VisitorState
import org.gls.lang.ImmutableLocation
import org.gls.lang.LocationFinder
import org.gls.lang.ReferenceStorage
import org.gls.lang.reference.ClassReference
import org.gls.lang.java.RangeMapper
import org.gls.lang.types.SimpleClass

@Slf4j
@TypeChecked
@ToString
class ClassDefinition implements Definition<ClassDefinition, ClassReference> {

    ImmutableLocation location

    SimpleClass type

    List<String> memberFunctions = []
    List<String> memberVariables = []

    ClassDefinition() {
    }

    @SuppressWarnings(["EmptyCatchBlock", "CatchException"])
    ClassDefinition(ClassNode node, String sourceFileURI, List<String> source) {
        try {
            memberFunctions = node.allDeclaredMethods*.name
            memberVariables = node.fields*.name
        } catch (Exception e1) {
        }

        type = new SimpleClass(name: node.name)
        this.location = LocationFinder.findLocation(sourceFileURI, source, node, type.nameWithoutPackage)
    }

    ClassDefinition(ClassOrInterfaceDeclaration node, VisitorState visitorState) {
        type = new SimpleClass(name: "${visitorState.packageName}.${node.name.toString()}")
        location = RangeMapper.map(visitorState.sourceFileURI.toString(), node.name.range)
    }

    @Override
    Set<ClassReference> findMatchingReferences(ReferenceStorage storage, Set<ClassDefinition> classDefinitions,
                                               Set<ClassReference> references) {
        references.findAll {
            it.type.toString() == type.toString()
        }
    }

}
