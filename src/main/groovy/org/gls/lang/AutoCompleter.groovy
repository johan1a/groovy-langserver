package org.gls.lang

import groovy.util.logging.Slf4j
import org.eclipse.lsp4j.CompletionItem
import org.eclipse.lsp4j.CompletionItemKind
import org.gls.lang.definition.ClassDefinition

/**
 * Created by johan on 4/23/18.
 */
@Slf4j
class AutoCompleter {

    private static final String DETAIL = "Language client detail"
    private static final String DOT = "."

    List<CompletionItem> autoComplete(ClassDefinition classDefinition, String precedingText) {
        if (!precedingText.contains(DOT)) {
            return []
        }
        log.debug("autoCompleting for classdef: $classDefinition, precedingText: $precedingText")

        // Class.forName(classDefinition.className).declaredMethods.findAll { !it.synthetic }.name.toSet()
        Set<String> functions = classDefinition.memberFunctions
        Set<String> variables = classDefinition.memberVariables

        String prefix
        if (precedingText.endsWith(DOT)) {
            prefix = ""
        } else {
            prefix = precedingText.split("\\.")[1]
        }

        List<CompletionItem> result = []
        functions.each {
            if (it.startsWith(prefix)) {
                CompletionItem item = new CompletionItem(it)
                item.kind = CompletionItemKind.Method
                item.detail = DETAIL
                result.add(item)
            }
        }
        variables.each {
            if (it.startsWith(prefix)) {
                CompletionItem item = new CompletionItem(it)
                item.kind = CompletionItemKind.Variable
                item.detail = DETAIL
                result.add(item)
            }
        }

        result
    }

}

