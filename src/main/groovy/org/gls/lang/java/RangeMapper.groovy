package org.gls.lang.java

import com.github.javaparser.Range
import org.eclipse.lsp4j.Position
import org.gls.lang.ImmutableLocation
import org.gls.lang.ImmutableRange

class RangeMapper {
    static ImmutableLocation map(String uri, Optional<Range> rangeOptional) {
        rangeOptional.map { range ->
            new ImmutableLocation(
                    uri,
                    new ImmutableRange(
                            new Position(range.begin.line - 1, range.begin.column - 1),
                            new Position(range.end.line - 1, range.end.column - 1)
                    )
            )
        }.orElse(null)
    }
}
