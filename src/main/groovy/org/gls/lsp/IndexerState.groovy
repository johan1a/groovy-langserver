package org.gls.lsp

import com.fasterxml.jackson.annotation.JsonIgnore
import org.gls.compile.Dependency
import org.gls.lang.ReferenceStorage

class IndexerState {
    @JsonIgnore
    ReferenceStorage jdkReferences = new ReferenceStorage()

    ReferenceStorage localReferences = new ReferenceStorage()
    List<Dependency> dependencies = []
}
