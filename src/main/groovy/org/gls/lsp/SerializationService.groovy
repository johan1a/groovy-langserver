package org.gls.lsp

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import groovy.json.JsonOutput
import groovy.util.logging.Slf4j
import org.gls.compile.ConfigService
import org.gls.lang.ReferenceStorage

/**
 * Created by johan on 5/26/18.
 */
@Slf4j
class SerializationService {

    public static final String INDEXER_STATE_FILE = "/indexer_state"
    public static final String REF_STORAGE_STATE_FILE = "/reference_state"

    static void serialize(URI directory, IndexerState state) {
        URI filePath = UriUtils.appendURI(directory, INDEXER_STATE_FILE)
        ObjectMapper mapper = new ObjectMapper()
        String output = JsonOutput.prettyPrint(mapper.writeValueAsString(state))
        File file = new File(filePath)
        file.createNewFile()
        file.text = output
    }

    static IndexerState deserialize(URI directory) {
        URI filePath = UriUtils.appendURI(directory, INDEXER_STATE_FILE)
        if (!new File(filePath).exists()) {
            log.info("Indexer state file not found at $filePath")
            return new IndexerState()
        }

        ObjectMapper mapper = deserializationObjectMapper
        IndexerState loadedState = mapper.readValue(filePath.toURL(), IndexerState)
        log.info("Indexer state loaded.")
        loadedState
    }

    static ReferenceStorage deserializeReferenceStorage(URI sourceDirectory) {
        URI directory = UriUtils.appendURI(ConfigService.BUNDLES_BASE_DIR, sourceDirectory)
        URI filePath = UriUtils.appendURI(directory, REF_STORAGE_STATE_FILE)
        if (!new File(filePath).exists()) {
            log.info("Indexer state file not found at $filePath")
            return new ReferenceStorage()
        }

        ObjectMapper mapper = deserializationObjectMapper
        mapper.serializationInclusion = NON_NULL
        ReferenceStorage loadedState = mapper.readValue(filePath.toURL(), ReferenceStorage)
        log.info("Reference storage loaded.")
        loadedState
    }

    String serializeReferenceStorage(URI sourceDirectory, ReferenceStorage storage) {
        URI directory = UriUtils.appendURI(ConfigService.BUNDLES_BASE_DIR, sourceDirectory)
        URI filePath = UriUtils.appendURI(directory, REF_STORAGE_STATE_FILE)
        log.info("serializing reference storage to $filePath")
        File directoryFile = new File(directory)
        if (!directoryFile.exists()) {
            directoryFile.mkdirs()
        }
        ObjectMapper mapper = new ObjectMapper()
        String output = JsonOutput.prettyPrint(mapper.writeValueAsString(storage))
        File file = new File(filePath)
        file.createNewFile()
        file.text = output
    }

    private static ObjectMapper getDeserializationObjectMapper() {
        ObjectMapper mapper = new ObjectMapper()
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        mapper.configure(DeserializationFeature.FAIL_ON_NULL_CREATOR_PROPERTIES, false)
        mapper.configure(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES, false)
        mapper
    }

    boolean refStorageFileExists(URI sourceDirectory) {
        URI directory = UriUtils.appendURI(ConfigService.BUNDLES_BASE_DIR, sourceDirectory)
        URI filePath = UriUtils.appendURI(directory, REF_STORAGE_STATE_FILE)
        new File(filePath).exists()
    }
}
