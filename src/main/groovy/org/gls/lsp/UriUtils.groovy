package org.gls.lsp

import org.eclipse.lsp4j.Position
import org.gls.lang.ImmutableLocation
import org.gls.lang.ImmutableRange

class UriUtils {

    private static final String DIR_SEPARATOR = "/"
    static final String FILE = "file://"
    static final String ZIP_FILE = "zipfile:"
    static final String FILE_SHORT = "file:"

    static URI appendURI(URI first, URI second) {
        return appendURI(first, second.toString())
    }

    static URI appendURI(URI uri, String fullPath) {
        String path = fullPath.replace(FILE, "").replace(FILE_SHORT, "")
        String uriString = uri
        if (uriString.endsWith(DIR_SEPARATOR) && path.startsWith(DIR_SEPARATOR)) {
            return new URI(uriString + path.substring(1))
        } else if (!uriString.endsWith(DIR_SEPARATOR) && !path.startsWith(DIR_SEPARATOR)) {
            return new URI(uriString + DIR_SEPARATOR + path)
        }
        return new URI(uriString + path)
    }

    /**
     *
     * @param bundleURI URI of the zip file
     * @param fileURI URI of the temp file
     * @return URI to the file inside of the zip file
     */
    static URI makeBundleURI(URI bundleURI, URI bundleTempDirURI, URI fileURI) {
        String bundleURIString = bundleURI
                .toString()
                .replace(FILE, ZIP_FILE)

        String strippedTempDir = bundleTempDirURI
                .toString()
                .replace(FILE, "")

        String fileInZipURIString = fileURI
                .toString()
                .replace(FILE_SHORT, "")
                .replace(strippedTempDir, "")

        new URI("${bundleURIString}::${fileInZipURIString}")
    }

    static List<ImmutableLocation> externalURIs(List<ImmutableLocation> locations) {
        locations.collect { location ->
            Position start = new Position(location.range.start.line, location.range.start.character)
            Position end = new Position(location.range.end.line, location.range.end.character)
            return new ImmutableLocation(externalURI(location), new ImmutableRange(start, end))
        }
    }

    private static String externalURI(ImmutableLocation location) {
        if (location.uri.startsWith(ZIP_FILE)) {
            location.uri
        } else {
            FILE + location.uri
        }
    }

    static URI parentDir(URI uri) {
        String[] splitted = uri.toString().split(DIR_SEPARATOR)
        new URI(splitted.take(splitted.size() - 1).join(DIR_SEPARATOR))
    }

    static URI toInternalUri(File file) {
        new URI(file.toURI().toString().replace(FILE_SHORT, ""))
    }
}
