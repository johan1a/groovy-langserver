package org.gls.lsp

import org.gls.lang.ReferenceStorage

class IndexingResult {

    IndexerState state

    Diagnostics diagnostics

    ReferenceStorage getStorage() {
        ReferenceStorage storage = new ReferenceStorage()
        storage.addAllReferences(state.jdkReferences)
        storage.addAllReferences(state.localReferences)
        storage
    }

}
