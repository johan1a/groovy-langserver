package org.gls.lsp

import groovy.util.logging.Slf4j
import org.eclipse.lsp4j.Diagnostic
import org.gls.compile.CompilerConfig
import org.gls.compile.ConfigService
import org.gls.compile.Dependency
import org.gls.compile.groovy.GroovyCompilerService
import org.gls.compile.java.JavaCompilerService
import org.gls.lang.LanguageService

@Slf4j
@SuppressWarnings(['CatchException'])
class Indexer {

    ConfigService configService = new ConfigService()

    IndexerState state = new IndexerState()

    SerializationService serializationService = new SerializationService()

    boolean shouldIndexJDK

    IndexingResult index(URI rootURI, CompilerConfig compilerConfig, Map<String, String> changedFiles = [:]) {
        log.info("Starting indexing")
        long start = System.currentTimeMillis()
        LanguageService languageService = new LanguageService()
        GroovyCompilerService compilerService = new GroovyCompilerService(rootURI, languageService, compilerConfig)

        List<Dependency> dependencies = getDependencies(rootURI, compilerConfig)
        Map<String, List<Diagnostic>> diagnostics = compilerService.compile(dependencies, changedFiles)
        state.localReferences = languageService.storage
        compilerConfig.scanDependencies = false

        if (shouldIndexJDK && state.jdkReferences.isEmpty()) {
            indexJDK(rootURI, compilerConfig)
        }
        languageService.addAllReferences(state.jdkReferences)

        BigDecimal elapsed = System.currentTimeMillis() - start
        log.info("Indexing finished in ${elapsed / 1000.0}s")
        new IndexingResult(diagnostics: new Diagnostics(diagnostics: diagnostics), state: state)
    }

    private void indexJDK(URI rootURI, CompilerConfig compilerConfig) {
        try {
            URI jdkSourceURI = configService.findJDKSourceURI()

            URI jdkSourceURIDir = new URI(jdkSourceURI.toString().replace("src.zip", ""))
            if (serializationService.refStorageFileExists(jdkSourceURIDir)) {
                log.info("Loading JDK references from file")
                state.jdkReferences = serializationService.deserializeReferenceStorage(jdkSourceURIDir)
            } else {
                log.info("Indexing JDK sources")
                LanguageService jdkLanguageService = new LanguageService()
                JavaCompilerService javaCompilerService = new JavaCompilerService(rootURI, jdkLanguageService,
                        compilerConfig)
                javaCompilerService.compileSourceBundle(jdkSourceURI)
                state.jdkReferences = jdkLanguageService.storage
                serializationService.serializeReferenceStorage(jdkSourceURIDir, state.jdkReferences)
            }

            shouldIndexJDK = false
            log.info("JDK indexing finished.")
        } catch (Exception e) {
            log.error("Error when indexing JDK", e)
        }
    }

    private List<Dependency> getDependencies(URI rootUri, CompilerConfig compilerConfig) {
        if (compilerConfig.scanDependencies) {
            if (state.dependencies.isEmpty()) {
                state.dependencies = configService.getDependencies(rootUri)
            }
            state.dependencies
        } else {
            return []
        }
    }
}
