package org.gls

import org.eclipse.lsp4j.Diagnostic
import org.eclipse.lsp4j.Position
import org.gls.compile.CompilerConfig
import org.gls.compile.ConfigService
import org.gls.compile.java.JavaCompilerService
import org.gls.lang.ImmutableLocation
import org.gls.lang.LanguageService
import org.gls.lang.definition.ClassDefinition
import spock.lang.Specification

@SuppressWarnings(["DuplicateStringLiteral", "Unused", "LineLength"])
class ZipFileExtractorSpec extends Specification {

    public static final String TEST_DIR = "file://${System.getProperty("user.dir")}/src/test/test-files/java/zip/"

    void "It should be able to parse the JDK"() {
        given:
            URI zipFileURI = new URI(TEST_DIR + "src.zip")

            LanguageService languageService = new LanguageService()

            JavaCompilerService compilerService = new JavaCompilerService(TEST_DIR.toURI(), languageService,
                    new CompilerConfig(scanAllSubDirs: true))

        when:
            Map<String, List<Diagnostic>> diagnostics = compilerService.compileSourceBundle(zipFileURI)

        then:
            diagnostics.isEmpty()
            Set<ClassDefinition> classDefinitions = languageService.storage.classDefinitions
            ClassDefinition definition = classDefinitions.find { it.type.name == "java.lang.String" }
            definition
            ImmutableLocation location = definition.location
            Position start = location.range.start
            start.line == 110
            start.character == 19

            String expected = "${ConfigService.CONFIG_BASE_DIR}/bundles".replace("file://", "") +
                    "${System.getProperty("user.dir")}/src/test/test-files/java/zip/java/lang/String.java"

            location.uri == expected
    }

}
