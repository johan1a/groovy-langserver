package org.gls

import org.gls.lsp.IndexerState
import org.gls.lsp.SerializationService
import org.gls.util.TestUtil
import spock.lang.Specification
import java.nio.file.Files
import java.nio.file.Paths

@SuppressWarnings(["LineLength", "TrailingWhitespace"])
class SerializationServiceSpec extends Specification {

    private static final String WORK_DIR = System.getProperty("user.dir")

    void "It should be able to serialize a LanguageService"() {
        given:
            String path = "/tmp/${UUID.randomUUID().toString()}"
            Files.createDirectory(Paths.get(path))
            URI rootUri = TestUtil.uri(path)
            IndexerState state = new IndexerState()
            SerializationService.serialize(rootUri, state)
            File file = new File("${path}/indexer_state")

            String expected = new File("${WORK_DIR}/src/test/test-files/config/indexer_state.expected").text

        expect:
            file.text == expected
    }

}
