package org.gls.compile

import static org.gls.util.TestUtil.testDeclaration

import spock.lang.Specification
import spock.lang.Unroll

@Unroll
@SuppressWarnings(["DuplicateStringLiteral", "DuplicateNumberLiteral", "DuplicateListLiteral"])
class VarDeclSpec extends Specification {

    void "Closure argument should result in Var declaration"() {
        given:
            String directory = 'small/vardecl0/'
            String file = 'VarRefInClosureArgumentWithoutType.groovy'
            List<Integer> position = [4, 40]
            List<Integer> expectedResultPosition = [4, 36]

        expect:
            testDeclaration(directory, file, position, expectedResultPosition)
    }

    void "Attributes should result in var def"() {
        given:
            String directory = 'small/varref7/'
            String file = 'Coco.groovy'
            List<Integer> position = _position
            List<Integer> expectedResultPosition = _expected

        expect:
            testDeclaration(directory, file, position, expectedResultPosition)

        where:
            _position | _expected
            [6, 33]   | [2, 11, "VarRefInAttribute.groovy"]
            [5, 50]   | [3, 18, "VarRefInAttribute.groovy"]
    }

}
