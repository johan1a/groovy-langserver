package org.gls.compile

import static org.gls.util.TestUtil.testReference

import spock.lang.Specification

@SuppressWarnings(["DuplicateNumberLiteral", "DuplicateListLiteral"])
class VarReferenceSpec extends Specification {

    void "Closure argument should result in Var reference"() {
        given:
            String directory = 'small/vardecl0/'
            String file = 'VarRefInClosureArgumentWithoutType.groovy'
            List<Integer> position = [4, 36]
            List<List<Integer>> expectedResultPositions = [[4, 36], [9, 19]]

        expect:
            testReference(directory, file, position, expectedResultPositions)
    }

    void "Attributes should result in var reference"() {
        given:
            String directory = 'small/varref7/'
            String file = 'VarRefInAttribute.groovy'
            List<Integer> position = [2, 11]
            List<List<Integer>> expectedResultPositions = [[2, 11], [6, 33, "Coco.groovy"]]

        expect:
            testReference(directory, file, position, expectedResultPositions)
    }

    void "Use URIs when comparing definition locations"() {
        given:
            String directory = 'small/varref8'
            String file = 'B.groovy'
            List<Integer> position = [6, 8]
            List<List<Integer>> expectedResultPositions = [[6, 8], [7, 16, "C.groovy"]]

        expect:
            testReference(directory, file, position, expectedResultPositions)
    }
}
