package org.gls.compile

import static org.gls.util.TestUtil.uri

import org.eclipse.lsp4j.Position
import org.gls.compile.groovy.GroovyCompilerService
import org.gls.lang.LanguageService
import org.gls.lang.definition.ClassDefinition
import spock.lang.Specification

@SuppressWarnings(["DuplicateNumberLiteral"])
class GroovyCompilerServiceSpec extends Specification {

    void "test indexer init"() {
        LanguageService finder = new LanguageService()
        URI uri = uri(".")

        GroovyCompilerService indexer = new GroovyCompilerService(uri, finder,
                new CompilerConfig(scanAllSubDirs: false))

        expect:
            indexer.sourcePaths.collect { it.toString().split(System.getProperty("user.dir"))[1] }
                    .containsAll(["/./src/main/groovy", "/./grails-app/domain"])
    }

    void "test indexer"() {
        LanguageService finder = new LanguageService()
        String path = "./src/test/test-files/1"

        GroovyCompilerService indexer = new GroovyCompilerService(uri(path), finder, new CompilerConfig())
        indexer.compile()

        expect:
            finder.storage.classDefinitions.size() == 1
    }

    void "Test unresolved import"() {
        setup:
            LanguageService languageService = new LanguageService()
            String path = "src/test/test-files/5"

        when:
            GroovyCompilerService compilerService = new GroovyCompilerService(uri(path), languageService,
                    new CompilerConfig())
            compilerService.compile()

        then:
            notThrown(Exception)
    }

    void "It should be able to handle joint compilation"() {
        setup:
            LanguageService languageService = new LanguageService()
            String path = "src/test/test-files/jointcompilation/basic"

        when:
            GroovyCompilerService compilerService = new GroovyCompilerService(uri(path), languageService,
                    new CompilerConfig())
            compilerService.compile()
            Set<ClassDefinition> definitions = languageService.storage.classDefinitions

        then:
            definitions.size() == 2
            ClassDefinition heyWoa = definitions.find { it.type.name == "basic.HeyWoa" }
            Position heyWoaStart = heyWoa.location.range.start
            heyWoaStart.line == 2
            heyWoaStart.character == 13

            ClassDefinition superSmash = definitions.find { it.type.name == "kickapoo.SuperSmash" }
            Position superSmashStart = superSmash.location.range.start
            superSmashStart.line == 4
            superSmashStart.character == 6
    }

}
