package org.gls.compile.java

import static org.gls.util.TestUtil.uri

import org.eclipse.lsp4j.Diagnostic
import org.eclipse.lsp4j.Position
import org.gls.compile.CompilerConfig
import org.gls.lang.LanguageService
import org.gls.lang.definition.ClassDefinition
import spock.lang.Specification

class JavaClassDefSpec extends Specification {

    void "Parse Java Class definition"() {
        given:
            LanguageService languageService = new LanguageService()
            String path = "src/test/test-files/java/basic"
            URI uri = uri(path)

            JavaCompilerService compilerService = new JavaCompilerService(uri, languageService,
                    new CompilerConfig(scanAllSubDirs: true))

        when:
            Map<String, List<Diagnostic>> diagnostics = compilerService.compile()

        then:
            diagnostics.isEmpty()
            Set classDefinitions = languageService.storage.classDefinitions
            classDefinitions.size() == 1
            ClassDefinition first = classDefinitions.toList().first()
            Position start = first.location.range.start
            start.line == 2
            start.character == 13
    }
}
