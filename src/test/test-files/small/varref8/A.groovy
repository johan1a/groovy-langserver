
class A {
    String packageName
    URI bundleURI
    URI tempFileURI
    URI bundleTempDirURI

    URI getSourceFileURI() {
        tempFileURI
    }

    List<String> fileContents
}
