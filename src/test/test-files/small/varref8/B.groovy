import org.gls.compile.Dependency

class B {
    List<URI> sourcePaths
    Map<String, String> changedFiles = [:]
    List<Dependency> dependencies = []
    URI bundleURI
    URI bundleTempDirURI
    List<File> files
}

